
import sbt._
import Keys._

object BuildGlobals {
  def projName(name: String = "") = "datamonster" + name
  val scalav = "2.10.3"
}

object HelloBuild extends Build {

  import BuildGlobals._

  lazy val datamonsterBaseCode = Project(
    id = projName("BaseCode"),
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := projName("BaseCode"),
      organization := projName(),
      scalaVersion := scalav)
  ).aggregate(datamonsterCrawler, datamonsterNLP, datamonsterSpark)

  lazy val datamonsterCrawler = Project(
    id = projName("Crawler"),
    base = file(projName("Crawler")),
    settings = Project.defaultSettings ++ Seq(
      name := projName("Crawler"),
      organization := projName(),
      scalaVersion := scalav
    )
  )

  lazy val datamonsterNLP = Project(
    id = projName("NLP"),
    base = file(projName("NLP")),
    settings = Project.defaultSettings ++ Seq(
      name := projName("NLP"),
      organization := projName(),
      scalaVersion := scalav,
      libraryDependencies := Seq(
        "cascading" % "cascading-core" % "2.5.1",
        "cascading" % "cascading-local" % "2.5.1",
        "cascading" % "cascading-hadoop" % "2.5.1",
        "com.twitter" % "scalding-core_2.10" % "0.9.0rc1",
        "org.apache.hadoop" % "hadoop-core" % "1.2.1"),
      resolvers := Seq("cascading" at "http://conjars.org/repo")
    )
  )

  lazy val datamonsterSpark = Project(
    id = projName("Spark"),
    base = file(projName("Spark")),
    settings = Project.defaultSettings ++ Seq(
      name := projName("Spark"),
      organization := projName(),
      scalaVersion := scalav,
      libraryDependencies := Seq(
        "org.apache.spark" % "spark-core_2.10" % "0.9.0-incubating")
    )
  )

}

